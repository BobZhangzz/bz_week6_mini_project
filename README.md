# BZ_week6_mini_project
 The obkective of this mini project is to instrument a Rust Lambda Function with Logging and Tracing

 Here are some key points to consider:
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Steps to complete the project
1. Create a new Rust Lambda function
```bash
cargo lambda new --name rust-lambda-logging-tracing
```
in the main.rs file, add the following code:
```rust
use lambda_http::{handler, lambda, Context, IntoResponse, Request};
use log::{error, info};
use simple_logger::SimpleLogger;

fn main() {
    SimpleLogger::new().init().unwrap();
    lambda!(handler);
}
```

2. Add logging to the Lambda function
Add the following dependencies to the Cargo.toml file:
```toml
[dependencies]
lambda_http = "0.2.0"
log = "0.4.14"
simple_logger = "1.11.0"
```

3. Integrate AWS X-Ray tracing
Add the following dependencies to the Cargo.toml file:
```toml
[dependencies]
lambda_runtime = "0.2.0"
tracing = "0.1.25"
tracing-subscriber = "0.2.15"
```

